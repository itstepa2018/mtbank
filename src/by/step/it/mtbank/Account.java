package by.step.it.mtbank;

public class Account {
    private static int idCounter;
    private int id;
    private double value;

    public Account() {
        this.id = idCounter++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
