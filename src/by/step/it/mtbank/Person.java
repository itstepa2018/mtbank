package by.step.it.mtbank;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private static int idCounter;
    private  int id;
    private String name;
    private List<Account> accountList;

    public Person(String name) {
        this.accountList = new ArrayList<>();
        this.id = idCounter++;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
